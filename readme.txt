=== Neuralab Test Update ===
Adds Neuralab branding to WordPress dashboard and login.

== Installation ==
Visit [the repo](https://bitbucket.org/neuralab/neuralab-branding/).

== Changelog ==

= 1.7 =
* Don't use composer.

= 1.6 =
* Use require for autoloader.

= 1.5 =
* Update Plugin Update Checker to 4.10.

= 1.4 =
* Use require_once to load vendors.

= 1.3 =
* Downgrade Plugin Update Checker to 4.9.

= 1.2 =
* Use composer for vendors.

= 1.1 =
* 2nd Release.

= 1.0 =
* First Release.
