<?php
/**
 * Plugin Name: Neuralab Test Update
 * Description: Test update library.
 * Version: 1.7
 * License: MIT License
 * Author: development@neuralab.net
 * Author URI: http://www.neuralab.net
 * Text Domain: nrlb-test-update
 */

defined( 'ABSPATH' ) || exit; // Exit if accessed directly.

require 'plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
  'https://slobich2000@bitbucket.org/slobich2000/test-update',
  __FILE__, //Full path to the main plugin file or functions.php.
  'nrlb-test-update'
);
